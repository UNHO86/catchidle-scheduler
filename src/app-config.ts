/**
 * Copyright 2017-2018. HalftimeStudio Inc. all rights reserved.
 */


'use strict';

import * as url from "url";
import * as util from "util";
import * as querystring from "querystring";
import MyURIParser from "./common/MyURIParser";

namespace TestAppConfig {
    // GAME DATABASE URI
    export const DATABASE_URI = 'mongodb://localhost:27017/test-catchidle';

}

namespace AppConfig {

    // Mongodb
    export const DATABASE_URI = process.env.DATABASE_URI || TestAppConfig.DATABASE_URI;

    // MEMBERSHIP IMAP URI
    export const MEMBERSHIP_IMAP_URI = 'imap://membership@halftimestudio.com:Halftime201!@imap.worksmobile.com:993?tls=true';

}

const parseData = MyURIParser(AppConfig.MEMBERSHIP_IMAP_URI);
console.log(parseData);

export default AppConfig;
