/**
 * Copyright 2017-2018. HalftimeStudio Inc. all rights reserved.
 */


'use strict';

import * as Imap from 'imap';
import {simpleParser} from "mailparser";
import {ImapMessageAttributes} from "imap";
import {ImapMessageBodyInfo} from "imap";

export interface IMyImapConfig extends Imap.Config {
}

function parseImapMessage(msg: Imap.ImapMessage) {
    return new Promise((resolve) => {
        let myMessage: any = {
            raw: null,
            attrs: null,
        };
        function messageOnBody(stream: NodeJS.ReadableStream, info: ImapMessageBodyInfo) {
            let body = '';
            function streamOnData(chunk: Buffer) {
                body += chunk.toString('utf8');
            }
            async function streamOnEnd() {
                stream.removeListener('data', streamOnData);
                myMessage.raw = body;
            }
            stream.on('data', streamOnData);
            stream.once('end', streamOnEnd);
        }

        function messageOnAttributes(attrs: ImapMessageAttributes) {
            myMessage.attrs = attrs;
        }

        function messageOnEnd() {
            msg.removeListener('body', messageOnBody);
            msg.removeListener('attributes', messageOnAttributes);
            resolve(myMessage);
        }

        msg.on('body', messageOnBody);
        msg.once('attributes', messageOnAttributes);
        msg.once('end', messageOnEnd);
    })
}

export class MyImap {
    protected _imap: Imap;

    public static async getMailServer(config: Imap.Config): Promise<MyImap> {
        const newImap = new MyImap(config);
        await newImap.initialize();
        return newImap;
    }

    public constructor(config: Imap.Config) {
        this._imap = new Imap(config);
    }

    protected connect(): Promise<void> {
        return new Promise<void>((resolve, reject) => {
            this._imap.once('error', (error) => reject(error));
            this._imap.once('ready', () => resolve());
            this._imap.connect();
        });
    }

    protected openBox(mailboxName: string = 'INBOX'): Promise<Imap.Box> {
        return new Promise<Imap.Box>((resolve, reject) => {
            this._imap.openBox(mailboxName, (error, mailbox) => {
                if (error) {
                    reject(error);
                } else {
                    resolve(mailbox);
                }
            });
        });
    }

    protected async initialize(): Promise<void> {
        await this.connect();
        await this.openBox()
    }

    public search(criteria: any[], options: Imap.FetchOptions) {
        return new Promise<any>((resolve, reject) => {
            this._imap.search(criteria, (error, uids) => {
                if (error) {
                    reject(error);
                    return;
                }

                if (!uids.length) {
                    resolve([]);
                    return;
                }

                const newFetch = this._imap.fetch(uids, options);
                const mailList = [];
                let doneCount = 0;

                async function fetchOnMessage(msg: Imap.ImapMessage, seqNo: number) {
                    const mail: any = await parseImapMessage(msg);
                    mail.data = await simpleParser(new Buffer(mail.raw, 'utf8'));
                    mailList.push(mail);
                    if (++doneCount === uids.length) {
                        fetchOnComplete();
                    }
                }

                function fetchOnComplete() {
                    resolve(mailList);
                }

                function fetchOnError(error) {
                    newFetch.removeListener('message', fetchOnMessage);
                    newFetch.removeListener('end', fetchOnEnd);
                    reject(error);
                }

                function fetchOnEnd() {
                    newFetch.removeListener('error', fetchOnError);
                    newFetch.removeListener('end', fetchOnEnd);
                }

                newFetch.on('message', fetchOnMessage);
                newFetch.once('error', fetchOnError);
                newFetch.once('end', fetchOnEnd);
            });
        });
    }

    public destory() {
        this._imap.destroy();
    }
}

export default MyImap;
