/**
 * Copyright 2017-2018. HalftimeStudio Inc. all rights reserved.
 */

'use strict';

import * as cron from 'node-cron';

interface IMyTaskInfo {
    taskName: string
    expression: string
    runFunc: () => void
    cronTask: cron.ScheduledTask
}

export class MyTaskManager {

    protected static _instance: MyTaskManager;

    public static get Instance() {
        if (!this._instance) {
            this._instance = new MyTaskManager();
        }
        return this._instance;
    }

    protected _taskMap: Map<string, IMyTaskInfo>;

    public constructor() {
        this._taskMap = new Map<string, IMyTaskInfo>();
    }

    public addTask(taskName: string, expression: string, runFunc: () => void): void {
        if (this._taskMap.has(taskName)) {
            throw new Error(`Exist Task: ${taskName}`);
        }

        if (!cron.validate(expression)) {
            throw new Error(`Invalid TimeExpression: ${taskName} ${expression}`);
        }

        const cronTask = cron.schedule(expression, runFunc);
        const myTask: IMyTaskInfo = {
            taskName,
            runFunc,
            expression,
            cronTask,
        };

        this._taskMap.set(taskName, myTask);
    }

    public start(): void {
        this._taskMap.forEach((myTask) => {
            myTask.cronTask.start();
        });
    }
}

export default MyTaskManager;
