/**
 * Copyright 2017-2018. HalftimeStudio Inc. all rights reserved.
 */

'use strict';

import {URL} from "url";

interface IMyURIData {
    /*
        nodejs doc: https://nodejs.org/docs/latest/api/url.html#url_class_url

        hash: Gets and sets the fragment portion of the URL.
        host: Gets and sets the host portion of the URL.
        hostname: Gets and sets the hostname portion of the URL. The key difference between url.host and url.hostname is that url.hostname does not include the port.
        href: Gets and sets the serialized URL.
        origin: Gets the read-only serialization of the URL's origin.
        password: Gets and sets the password portion of the URL.
        pathname: Gets and sets the path portion of the URL.
        port: Gets and sets the port portion of the URL.
        protocol: Gets and sets the protocol portion of the URL.
        search: Gets and sets the serialized query portion of the URL.
        searchParams: Gets the URLSearchParams object representing the query parameters of the URL. This property is read-only; to replace the entirety of query parameters of the URL, use the url.search setter. See URLSearchParams documentation for details.
        username: Gets and sets the username portion of the URL.
    */
    hash: string
    host: string
    hostname: string
    href: string
    origin: string
    password: string
    pathname: string
    port: string
    protocol: string
    search: string
    username: string
    query: { [key: string]: string }
}

function MyURIParser(uniformResourceIdentifier: string): IMyURIData {
    const newURL = new URL(uniformResourceIdentifier);
    const myURIData: IMyURIData = {
        hash: newURL.hash,
        host: newURL.host,
        hostname: newURL.hostname,
        href: newURL.href,
        origin: newURL.origin,
        password: newURL.password,
        pathname: newURL.pathname,
        port: newURL.port,
        protocol: newURL.protocol,
        search: newURL.search,
        username: newURL.username,
        query: {},
    };
    newURL.searchParams.forEach((v, n) => {
        myURIData.query[n] = v;
    });
    return myURIData;
}

export default MyURIParser;
