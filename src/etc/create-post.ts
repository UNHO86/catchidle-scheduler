/**
 * Copyright 2017-2018. HalftimeStudio Inc. all rights reserved.
 */


'use strict';
import * as fs from "fs";
import * as moment from 'moment';

// 메시지 발송 출처
export enum PostboxFrom { None = 0, OverGet = 1, Membership = 2, Coupon = 3, Default = 4, AdminDefault = 5, Event = 6, Admin = 7 }
export enum GetFrom { None = 0, Default = 1, CharShop = 2, EquipShop = 3, HonorShop = 4, MysteryShop = 5, TimeShop = 6, EventShop = 7, CharGacha = 8, EquipGacha = 9, PackageShopReward = 10, PackageShop = 11, AttendReward = 12, ContAttendReward = 13, DailyMission = 14, DailyMissionPoint = 15, Dungeon = 16, StageVIPReward = 17, BattleVIPReward = 18, DungeonVIPReward = 19, PvPDailyReward = 20, PvPSeasonReward = 21, DailyReward = 22, EventShopMission = 23, MysteryShopPoint = 24, RebirthStone = 25, SeasonSoulAccuRankReward = 26, SeasonBestStageRankReward = 27, Capsule = 28, ServerCheck = 29, Update = 30, Error = 31, CashShop = 32, PremiumShop = 33, AchieveReward = 34, CharCollectionReward = 35, EquipCollectionReward = 36, PvPBattleReward = 37, StoryReward = 38, DevStoryReward = 39, TreasureReward = 40, EventReward = 41, HelpReward = 42, StageChestReward = 43, Membership = 44, Coupon = 45, Event = 46, Admin = 47, AdReward = 48, EquipMake = 49, DungeonBoss = 50, DungeonBossShop = 51, PeriodShop = 52, LevelPackageShop = 53, AttendPackageShop = 54 }
export enum RewardType { None = 0, Gold = 1, Cash = 2, Soul = 3, Honor = 4, CharId = 5, TreasureId = 6, PvPTicket = 7, DungeonAutoTicket = 8, CharGachaTicket = 9, EquipGachaTicket = 10, Material1 = 11, Material2 = 12, Material3 = 13, Material4 = 14, Material5 = 15, Material6 = 16, EquipId = 17, CharGradeFixTicket5 = 18, CharGradeFixTicket6 = 19, EquipGradeFixTicket5 = 20, EquipGradeFixTicket6 = 21, ServerCharId = 22, ServerEquipId = 23, VIPPoint = 24, EmblemId = 25, Buff = 26, MasteryResetTicket = 27, CharTranscendStone = 28, NormalPowder = 29, SpecialPowder = 30, LegendPowder = 31, BossToken = 32, Orb = 33, ArcheologyExp = 34, DungeonBossTicket = 35, PackageBonus = 36 }

const postboxStringify = `{
    "_id" : "_ID",
    "GetTime" : ISODate("1970-01-01T00:00:00.000Z"),
    "RecvTime" : ISODate("_RECV_TIME"),
    "RewardList" : _POST_REWARD_INFO_LIST,
    "IsGet" : false,
    "KeepDays" : -1,
    "Desc" : _DESCRIPTION,
    "UserId" : "_USER_ID",
    "From" : _POSTBOX_FROM,
    "GetFrom" : _GET_FROM,
    "_wperm" : [],
    "_rperm" : [
        "_USER_ID"
    ],
    "_acl" : {
        "_USER_ID" : {
            "r" : true
        }
    },
    "_created_at" : ISODate("_RECV_TIME"),
    "_updated_at" : ISODate("_RECV_TIME")
}`;

let seq = 0;
const Desc = JSON.stringify(JSON.stringify({
    // EN: 'Thank you for joining HalftimeStudio Membership!\n\nNow you received 3 transcend tickets in your Mailbox.\nWe will offer you information including update, events, etc first.\nThank you!',
    KO: '미지급된 기간제 상품이 지급되었습니다.\n이용에 불편을 드려 죄송합니다.',
}));
const PostRewardInfoList = JSON.stringify(JSON.stringify([
    {
        "Type": 26,
        "Value": "GameSpeed_10080_0_2"
    },
    {
        "Type": 26,
        "Value": "GameAuto_10080_0_1"
    },
    {
        "Type": 26,
        "Value": "Cash_10080_0_7"
    }
]));
const UserIdList = [
    'ukpXwLrI4h',
];

function pad(value: number, padString = '000') {
    return (padString + value).slice(-padString.length); // returns 00123
}

const nowMoment = moment();
const exportFileName = nowMoment.format('POSTBOX_YYYYMMDD_HHmmss');

for (let i = 0; i < UserIdList.length; i++) {
    const newPost = postboxStringify
        .replace(/_RECV_TIME/g, nowMoment.toISOString())
        .replace(/_USER_ID/g, UserIdList[i])
        .replace(/_DESCRIPTION/g, Desc)
        .replace(/_POST_REWARD_INFO_LIST/g, PostRewardInfoList)
        .replace(/_POSTBOX_FROM/g, PostboxFrom.Admin.toString())
        .replace(/_GET_FROM/g, GetFrom.Admin.toString())
        .replace(/_ID/g, 'A' + nowMoment.format('MMDDHH') + pad(++seq));
    fs.appendFileSync(exportFileName, newPost + ',');
}
