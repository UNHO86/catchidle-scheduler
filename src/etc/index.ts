import * as fs from "fs";
const charTable = fs.readFileSync('CharTable.csv', 'utf8');
const charNameTable = fs.readFileSync('Localization - CharName.csv', 'utf8');
const targetType = 'Move';
const charList = charTable.split('\n');
const nameList = charNameTable.split('\n');
const nameMap = {};
for (const name of nameList) {
    const nameInfo = name.split(',');
    nameMap[nameInfo[0]] = nameInfo[2];
}

for (const char of charList) {
    const charInfo = char.split(',');
    if (charInfo[40].indexOf(targetType) !== -1) console.log(`evolve ${charInfo[40]} ${charInfo[40 + 10]} ${charInfo[0]} ${charInfo[2]} ${charInfo[4]} ${charInfo[7]} ${nameMap[charInfo[0]]}`);
    if (charInfo[41].indexOf(targetType) !== -1) console.log(`evolve ${charInfo[41]} ${charInfo[41 + 10]} ${charInfo[0]} ${charInfo[2]} ${charInfo[4]} ${charInfo[7]} ${nameMap[charInfo[0]]}`);
    if (charInfo[42].indexOf(targetType) !== -1) console.log(`evolve ${charInfo[42]} ${charInfo[42 + 10]} ${charInfo[0]} ${charInfo[2]} ${charInfo[4]} ${charInfo[7]} ${nameMap[charInfo[0]]}`);
    if (charInfo[43].indexOf(targetType) !== -1) console.log(`evolve ${charInfo[43]} ${charInfo[43 + 10]} ${charInfo[0]} ${charInfo[2]} ${charInfo[4]} ${charInfo[7]} ${nameMap[charInfo[0]]}`);
    if (charInfo[44].indexOf(targetType) !== -1) console.log(`evolve ${charInfo[44]} ${charInfo[44 + 10]} ${charInfo[0]} ${charInfo[2]} ${charInfo[4]} ${charInfo[7]} ${nameMap[charInfo[0]]}`);
    if (charInfo[62].indexOf(targetType) !== -1) console.log(`transcend 1 ${charInfo[62]} ${charInfo[62 + 12]} ${charInfo[0]} ${charInfo[2]} ${charInfo[4]} ${charInfo[7]} ${nameMap[charInfo[0]]}`);
    if (charInfo[63].indexOf(targetType) !== -1) console.log(`transcend 1 ${charInfo[63]} ${charInfo[63 + 12]} ${charInfo[0]} ${charInfo[2]} ${charInfo[4]} ${charInfo[7]} ${nameMap[charInfo[0]]}`);
    if (charInfo[64].indexOf(targetType) !== -1) console.log(`transcend 2 ${charInfo[64]} ${charInfo[64 + 12]} ${charInfo[0]} ${charInfo[2]} ${charInfo[4]} ${charInfo[7]} ${nameMap[charInfo[0]]}`);
    if (charInfo[65].indexOf(targetType) !== -1) console.log(`transcend 2 ${charInfo[65]} ${charInfo[65 + 12]} ${charInfo[0]} ${charInfo[2]} ${charInfo[4]} ${charInfo[7]} ${nameMap[charInfo[0]]}`);
    if (charInfo[66].indexOf(targetType) !== -1) console.log(`transcend 3 ${charInfo[66]} ${charInfo[66 + 12]} ${charInfo[0]} ${charInfo[2]} ${charInfo[4]} ${charInfo[7]} ${nameMap[charInfo[0]]}`);
    if (charInfo[67].indexOf(targetType) !== -1) console.log(`transcend 3 ${charInfo[67]} ${charInfo[67 + 12]} ${charInfo[0]} ${charInfo[2]} ${charInfo[4]} ${charInfo[7]} ${nameMap[charInfo[0]]}`);
    if (charInfo[80].indexOf(targetType) !== -1) console.log(`lv up 1 ${charInfo[80]} ${charInfo[80 + 30]} ${charInfo[0]} ${charInfo[2]} ${charInfo[4]} ${charInfo[7]} ${nameMap[charInfo[0]]}`);
    if (charInfo[81].indexOf(targetType) !== -1) console.log(`lv up 2 ${charInfo[81]} ${charInfo[81 + 30]} ${charInfo[0]} ${charInfo[2]} ${charInfo[4]} ${charInfo[7]} ${nameMap[charInfo[0]]}`);
    if (charInfo[82].indexOf(targetType) !== -1) console.log(`lv up 3 ${charInfo[82]} ${charInfo[82 + 30]} ${charInfo[0]} ${charInfo[2]} ${charInfo[4]} ${charInfo[7]} ${nameMap[charInfo[0]]}`);
    if (charInfo[83].indexOf(targetType) !== -1) console.log(`lv up 4 ${charInfo[83]} ${charInfo[83 + 30]} ${charInfo[0]} ${charInfo[2]} ${charInfo[4]} ${charInfo[7]} ${nameMap[charInfo[0]]}`);
    if (charInfo[84].indexOf(targetType) !== -1) console.log(`lv up 5 ${charInfo[84]} ${charInfo[84 + 30]} ${charInfo[0]} ${charInfo[2]} ${charInfo[4]} ${charInfo[7]} ${nameMap[charInfo[0]]}`);
    if (charInfo[85].indexOf(targetType) !== -1) console.log(`lv up 6 ${charInfo[85]} ${charInfo[85 + 30]} ${charInfo[0]} ${charInfo[2]} ${charInfo[4]} ${charInfo[7]} ${nameMap[charInfo[0]]}`);
    if (charInfo[86].indexOf(targetType) !== -1) console.log(`lv up 7 ${charInfo[86]} ${charInfo[86 + 30]} ${charInfo[0]} ${charInfo[2]} ${charInfo[4]} ${charInfo[7]} ${nameMap[charInfo[0]]}`);
    if (charInfo[87].indexOf(targetType) !== -1) console.log(`lv up 8 ${charInfo[87]} ${charInfo[87 + 30]} ${charInfo[0]} ${charInfo[2]} ${charInfo[4]} ${charInfo[7]} ${nameMap[charInfo[0]]}`);
    if (charInfo[88].indexOf(targetType) !== -1) console.log(`lv up 9 ${charInfo[88]} ${charInfo[88 + 30]} ${charInfo[0]} ${charInfo[2]} ${charInfo[4]} ${charInfo[7]} ${nameMap[charInfo[0]]}`);
    if (charInfo[89].indexOf(targetType) !== -1) console.log(`lv up 10 ${charInfo[89]} ${charInfo[89 + 30]} ${charInfo[0]} ${charInfo[2]} ${charInfo[4]} ${charInfo[7]} ${nameMap[charInfo[0]]}`);
    if (charInfo[90].indexOf(targetType) !== -1) console.log(`lv up 11 ${charInfo[90]} ${charInfo[90 + 30]} ${charInfo[0]} ${charInfo[2]} ${charInfo[4]} ${charInfo[7]} ${nameMap[charInfo[0]]}`);
    if (charInfo[91].indexOf(targetType) !== -1) console.log(`lv up 12 ${charInfo[91]} ${charInfo[91 + 30]} ${charInfo[0]} ${charInfo[2]} ${charInfo[4]} ${charInfo[7]} ${nameMap[charInfo[0]]}`);
    if (charInfo[92].indexOf(targetType) !== -1) console.log(`lv up 13 ${charInfo[92]} ${charInfo[92 + 30]} ${charInfo[0]} ${charInfo[2]} ${charInfo[4]} ${charInfo[7]} ${nameMap[charInfo[0]]}`);
    if (charInfo[93].indexOf(targetType) !== -1) console.log(`lv up 14 ${charInfo[93]} ${charInfo[93 + 30]} ${charInfo[0]} ${charInfo[2]} ${charInfo[4]} ${charInfo[7]} ${nameMap[charInfo[0]]}`);
    if (charInfo[94].indexOf(targetType) !== -1) console.log(`lv up 15 ${charInfo[94]} ${charInfo[94 + 30]} ${charInfo[0]} ${charInfo[2]} ${charInfo[4]} ${charInfo[7]} ${nameMap[charInfo[0]]}`);
}
