/*
import * as imap from "imap-simple";
import * as mongodb from "mongodb";
import {simpleParser} from "mailparser";
import * as moment from "moment";
import {Iconv} from "iconv";
import {isString} from "util";

// const _DATABASE_URI = 'mongodb://catchidle-west-eu:Zw3fz9KpNph44w$v@ds012498-a0.dfk53.fleet.mlab.com:12498,ds012498-a1.dfk53.fleet.mlab.com:12496/catchidle-west-eu?replicaSet=rs-ds012498';
const _DATABASE_URI = 'mongodb://localhost:27017/test';
const _MEMBERSHIP_COLLECTION = 'MembershipUser';
const _GD_Postbox_COLLECTION = 'GD_Postbox';
const _GD_UserInfo_COLLECTION = 'GD_UserInfo';

const config = {
    // mongodb_uri: 'http://localhost:27017/',
    imap: {
        user: 'membership@halftimestudio.com',
        password: 'Halftime201!',
        host: 'imap.worksmobile.com',
        port: 993,
        tls: true,
    }
};

namespace slack {
    export async function log(message: string) {
        console.log(message);
        // await SlackNotice.instance.sendNotice(message, '@unho', 'Membership')
    }

    export async function error(message: string) {
        console.error(message);
        // await SlackNotice.instance.sendNotice(message, '@unho', 'Error-Membership')
    }
}


namespace MyMail {
    export async function getMailServer(): Promise<imap.ImapSimple> {
        const mailServer = await imap.connect(config);
        await mailServer.openBox('INBOX');
        return mailServer;
    }

    export interface IMyMailMessageStruct {
        encoding: string
        params: {
            charset: string
        }
        size: number
        type: string
    }

    export interface IMyMailMessage {
        uid: number
        raw: string
        text: string
        html: string
        address: string
        name: string
        date: Date
    }

    export async function getNewMailList(mailServer: imap.ImapSimple): Promise<IMyMailMessage[]> {
        const mailList = await mailServer.search(["NEW"], {bodies: [''], struct: true});
        const result = [];
        for (const mail of mailList) {
            const convMail = await simpleParser(mail.parts[0].body.toString('binary'));
            result.push({
                uid: mail.attributes.uid,
                raw: mail.parts[0].body,
                address: convMail.from.value[0].address,
                name: convMail.from.value[0].name,
                text: convMail.text,
                html: convMail.html,
                date: convMail.date,
            });
        }
        return result;
    }

    export async function setSeenFlag(mailServer: imap.ImapSimple, uid: number): Promise<void> {
        return mailServer.addFlags(uid.toString(), ['Seen']);
    }

    export function parseMyMailText(struct: IMyMailMessageStruct, text: string): string {
        const buffer = new Buffer(text, struct.encoding);
        if (struct.params.charset && struct.params.charset !== 'utf-8') {
            const iconv = new Iconv(struct.params.charset, 'utf8');
            return iconv.convert(buffer).toString('utf8');
        } else {
            return buffer.toString('utf8');

        }
    }
}

namespace MemberShipTool {
    export function getUserId(mailBody: string): string {
        try {
            const matchRows = mailBody.match(/User Key[ |:|\n|\r]+\S*!/);
            if (matchRows) {
                const base64UserKey = (matchRows[0].split(/[ |:|\n|\r]/g)).pop();
                const userKey = new Buffer(base64UserKey, 'base64').toString();
                return userKey.split('#')[0];
            }
        } catch {

        }
    }

    function padNumber(value: number, padString = '00000') {
        return (padString + value).slice(-padString.length); // returns 00123
    }

    export function createRewardPost(uid: number, userId: string, nowMoment: moment.Moment = moment()) {
        // PDgUlpFoq0
        // MBR0
        const _id = 'MBR0' + padNumber(uid);
        const UserId = userId;
        const minDate = new Date(0);
        const nowDate = nowMoment.toDate();
        return {
            _id,
            GetTime: minDate,
            RecvTime: nowDate,
            RewardList: "[{\"Type\":28,\"Value\":\"3\"}]",
            IsGet: false,
            KeepDays: -1,
            Desc: "{\"EN\":\"Thank you for joining HalftimeStudio Membership!\\n\\nNow you received 3 transcend tickets in your Mailbox.\\nWe will offer you information including update, events, etc first.\\nThank you!\",\"KO\":\"멤버십 가입을 환영합니다.\\n\\n약속드린 초월권 3개를 지급해드립니다.\\n향후 하프타임 스튜디오에서 제공하는\\n서비스 이용에 가장 먼저 정보를 전달 드리겠습니다.\\n감사합니다.\"}",
            UserId,
            From: 2,
            GetFrom: 46,
            _wperm: [],
            _rperm: [
                UserId
            ],
            "_acl": {
                [UserId]: {
                    "r": true
                }
            },
            _created_at: nowDate,
            _updated_at: nowDate,
        }
    }
}

async function parseMail(mail: MyMail.IMyMailMessage) {
    const uid = mail.uid;
    const Email = mail.address;
    const Nick = mail.name;
    let UserId: string;

    if (!UserId && mail.text) {
        UserId = MemberShipTool.getUserId(mail.text);
    }
    if (!UserId && mail.html && isString(mail.html)) {
        UserId = MemberShipTool.getUserId(mail.html);
    }
    if (!UserId && mail.raw) {
        UserId = MemberShipTool.getUserId(mail.raw);
    }

    return {
        uid,
        Email,
        Nick,
        UserId,
    }
}

async function main() {
    const db = await mongodb.MongoClient.connect(_DATABASE_URI);
    const mailServer = await MyMail.getMailServer();
    const mailList = await MyMail.getNewMailList(mailServer);
    const nowMoment = moment();
    for (const mail of mailList) {
        const uid = mail.uid;
        const userInfo = await parseMail(mail);
        if (userInfo.UserId) {
            const exist = await db.collection(_MEMBERSHIP_COLLECTION).findOne({$or: [{UserId: userInfo.UserId}, {Email: userInfo.Email}]});
            if (exist) {
                await slack.error(`exist member: ${exist._create_at.toISOString()} ${exist.UserId} ${exist.Email} <> ${userInfo.UserId}, ${userInfo.Email}`);
            } else {
                const updateUserInfo = await db.collection(_GD_UserInfo_COLLECTION).updateOne({UserId: userInfo.UserId}, {$set: {Membership: true}});
                if (updateUserInfo.result.n > 0) {
                    await db.collection(_MEMBERSHIP_COLLECTION).insertOne({
                        UserId: userInfo.UserId,
                        Email: userInfo.Email,
                        Nick: userInfo.Nick,
                        _create_at: nowMoment.toDate()
                    });
                    const newPost = MemberShipTool.createRewardPost(uid, userInfo.UserId, nowMoment);
                    const ret = await db.collection(_GD_Postbox_COLLECTION).insertOne(newPost);
                    console.log(ret.result);
                } else {
                    await slack.error(`unknown user ${userInfo.UserId} ${userInfo.Email}`);
                }
            }
        } else {
            await slack.error('Invalid UserKey : ' + mail.raw);
        }
        // await MyMail.setSeenFlag(mailServer, uid);
    }
    await slack.log('done: ' + mailList.length);
}

main().then(() => {
    console.log('done');
    process.exit();
}).catch((err) => {
    return slack.error(err).then(process.exit(1));
});

*/
