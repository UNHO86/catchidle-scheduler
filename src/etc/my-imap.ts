/*
'use strict';

import * as Imap from 'imap';
import * as util from "util";
import {simpleParser} from "mailparser";
import * as fs from "fs";

class MyImap {
    _imap: Imap;
    _connected: boolean;

    public static async getConnection(config: Imap.Config): Promise<MyImap> {
        const newMyImap = new MyImap(config);
        await newMyImap.Connect();
        return newMyImap;
    }

    public constructor(config: Imap.Config) {
        this._imap = new Imap(config);
        this._connected = false;
    }

    public Connect(): Promise<void> {
        if (this._connected) return;
        return new Promise<void>((resolve, reject) => {
            this._imap.removeAllListeners();
            /!*
                ready () - 서버에 연결되어 인증에 성공하면 출력됩니다.
                alert (<string> message) - 서버에서 경고를 보낼 때 발생합니다 (예 : "유지 관리를 위해 서버가 다운 됨").
                mail (<integer> numNewMsgs) - 현재 열려있는 편지함에 새 메일이 도착하면 방출됩니다.
                expunge (<integer> seqno) - 메시지가 외부에서 제거 된 경우에 발생합니다. seqno는 소거 된 메시지의 순차 번호 (고유 UID 대신)입니다. 순차 번호를 캐싱하는 경우, 서버와의 동기화 상태를 유지하고 올바른 연속성을 유지하려면이 값보다 높은 모든 순차 번호를 1 씩 감소시켜야합니다 (MUST).
                uidvalidity (<integer> uidvalidity) - 현재 세션 동안 현재 열려있는 사서함의 UID 유효성 값이 변경되면 발생합니다.
                update (<integer> seqno, <object> info) - 메시지 메타 데이터 (예 : 플래그)가 외부에서 변경 될 때 발생합니다.
                error (<Error> err) - 오류가 발생하면 발생합니다. 'source'속성은 오류가 발생한 위치를 나타내도록 설정됩니다.
                close (<boolean> hasError) - 연결이 완전히 닫힌 경우에 발생합니다.
                end () - 연결이 종료되면 발생합니다.
            *!/
            this._imap.once('ready', () => {
                resolve();
            });
            this._imap.once('error', (err) => {
                reject(err);
            });
            this._imap.once('close', () => {
                this._connected = false;
            });
            this._imap.connect();
        });
    }

    public Destroy(): void {
        this._imap.destroy();
        this._connected = false;
    }

    public OpenBox(boxName: string): Promise<void> {
        return new Promise<void>((resolve, reject) => {
            this._imap.openBox(boxName, (err) => {
                if (err) {
                    reject(err);
                } else {
                    resolve();
                }
            });
        })
    }

    private streamReader(stream: NodeJS.ReadableStream): Promise<string> {
        return new Promise<string>((resolve) => {
            let buff = '';
            function streamOnData(chunk) {
                buff += chunk.toString('binary');
            }
            stream.on('data', streamOnData);
            stream.once('end', () => {
                stream.removeListener('data', streamOnData);
                resolve(buff.toString('utf8'));
            });
        });
    }

    public Search(criteria: any[], fetchOptions: Imap.FetchOptions): Promise<Imap.ImapMessage[]> {
        return new Promise<Imap.ImapMessage[]>((resolve, reject) => {
            this._imap.search(criteria, (error, uids) => {
                if (error) {
                    return reject(error);
                }
                if (!uids.length) {
                    return resolve([]);
                }

                const newFetch = this._imap.fetch(uids, fetchOptions);
                let messagesRetrieved = 0;
                const messages: Imap.ImapMessage[] = [];
                newFetch.on('message', (msg, seqno) => {
                    const newMsg: any = {};
                    msg.on('body', async (stream, info) => {
                        newMsg.raw = await this.streamReader(stream);
                        newMsg.body = await simpleParser(newMsg.raw);
                        newMsg.rawInfo = info;
                        messages.push(newMsg);
                        if (++messagesRetrieved === uids.length) {
                            resolve(messages);
                        }
                    });
                    msg.once('attributes', function (attrs) {
                        newMsg.attrs = attrs;
                    });
                    msg.once('end', function () {
                    });
                });
                newFetch.once('error', (fetchError) => {
                    newFetch.removeAllListeners();
                    reject(fetchError);
                });
                newFetch.once('end', () => {
                    newFetch.removeAllListeners();
                })
            })
        });
    }
}

function getConnection(config: Imap.Config): Promise<Imap> {
    return new Promise<Imap>((resolve, reject) => {
        const newConn = new Imap(config);
        newConn.once('ready', () => resolve(newConn));
        // newConn.on('alert', () => {});
        // newConn.on('mail', () => {});
        // newConn.on('expunge', () => {});
        // newConn.on('uidvalidity', () => {});
        // newConn.on('update', () => {});
        // newConn.on('error', () => {});
        // newConn.on('close', () => {});
        // newConn.on('end', () => {});
        newConn.once('error', (err) => {
            newConn.removeAllListeners();
            reject(err);
        });
    });
}

async function main() {
    const mailServer = await MyImap.getConnection({
        user: 'membership@halftimestudio.com',
        password: 'Halftime201!',
        host: 'imap.worksmobile.com',
        port: 993,
        tls: true,
    });
    await mailServer.OpenBox('INBOX');
    const mailList = await mailServer.Search(['UNSEEN'], {bodies: [''], struct: true});

    console.log('done');
}

main();

//
// function openInbox(cb) {
//     imap.openBox('INBOX', true, cb);
// }
//
// imap.once('ready', function() {
//
//     openInbox(function (err, box) {
//         if (err) throw err;
//         imap.search(['UNSEEN'], function (err, results) {
//             if (err) throw err;
//             var f = imap.fetch(results, {bodies: ''});
//             f.on('message', function (msg, seqno) {
//                 console.log('Message #%d', seqno);
//                 var prefix = '(#' + seqno + ') ';
//                 msg.on('body', function (stream, info) {
//                     console.log(prefix + 'Body');
//                     // stream.pipe(fs.createWriteStream('msg-' + seqno + '-body.txt'));
//                 });
//                 msg.once('attributes', function (attrs) {
//                     // console.log(prefix + 'Attributes: %s', inspect(attrs, false, 8));
//                 });
//                 msg.once('end', function () {
//                     console.log(prefix + 'Finished');
//                 });
//             });
//             f.once('error', function (err) {
//                 console.log('Fetch error: ' + err);
//             });
//             f.once('end', function () {
//                 console.log('Done fetching all messages!');
//                 imap.end();
//             });
//         });
//     });
// });
// imap.connect();
*/
