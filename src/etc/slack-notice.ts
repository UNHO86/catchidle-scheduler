/*
'use strict';
import * as Slack from 'slack-node';
// import AppConfig from '../../app.AppConfig';

class SlackNotice {
    private static _instance: SlackNotice = new SlackNotice();
    private _slack: Slack;
    private _default_sender: string;

    constructor () {
        this._slack = new Slack();
        this._slack.setWebhook('https://hooks.slack.com/services/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');
        this._default_sender = 'CatchIdle';
    }

    public static get instance (): SlackNotice {
        return SlackNotice._instance as any;
    }

    public async sendNotice (text: string, channel: string, username: string = this._default_sender): Promise<void> {
        return new Promise<void>((resolve, reject) => {
            this._slack.webhook({channel, text, username}, (err) => {
                if (err) {
                    reject(err);
                } else {
                    resolve();
                }
            });
        });
    }
}

export default SlackNotice;

*/
