/**
 * Copyright 2017-2018. HalftimeStudio Inc. all rights reserved.
 */


'use strict';
import MyTaskManager from "./common/MyTaskManager";
import CronJobMembership from "./tasks/cron-membership";
import CronPvPEnd from "./tasks/cron-pv-p-end";
MyTaskManager.Instance.addTask('Membership', '* * * * * *', CronJobMembership);
MyTaskManager.Instance.addTask('PvPEnd', '* * * * * *', CronPvPEnd);
MyTaskManager.Instance.addTask('SeasonEnd', '* * * * * *', () => {console.log(3)});
MyTaskManager.Instance.start();
CronJobMembership();