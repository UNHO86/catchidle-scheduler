/**
 * Copyright 2017-2018. HalftimeStudio Inc. all rights reserved.
 */


'use strict';

import MyImap from "../common/MyImap";
import AppConfig from "../app-config";
import MyURIParser from "../common/MyURIParser";

async function CronMembership() {
    const uriData = MyURIParser(AppConfig.MEMBERSHIP_IMAP_URI);
    const config = {
        user: decodeURIComponent(uriData.username),
        password: decodeURIComponent(uriData.password),
        host: decodeURIComponent(uriData.hostname),
        port: parseInt(uriData.port),
        tls: !!uriData.query.tls,
    };
    const mailServer = await MyImap.getMailServer(config);
    const messages = await mailServer.search(['UNSEEN'], {bodies: [''], struct: true});
    for (const message of messages) {
    }
    mailServer.destory();
    console.log('done');
}

export default CronMembership;